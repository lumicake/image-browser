import { handleActions } from 'redux-actions';
import {
  FETCH_ALBUM_PHOTOS_FAILURE,
  FETCH_ALBUM_PHOTOS_REQUEST,
  FETCH_ALBUM_PHOTOS_SUCCESS,
  FETCH_ALBUMS_FAILURE,
  FETCH_ALBUMS_REQUEST,
  FETCH_ALBUMS_SUCCESS,
  SET_ALBUM_CURRENT_PAGE,
  SET_ALBUMS_CURRENT_PAGE
} from '../actions/types';
import { DEFAULT_PAGE } from '../config';

const albumsReducer = handleActions({
  [FETCH_ALBUMS_REQUEST]: (state) => ({
    ...state,
    isFetchingAlbums: true,
    error: null
  }),
  [FETCH_ALBUMS_SUCCESS]: (state, { payload }) => ({
    ...state,
    ...payload,
    albumCurrentPage: DEFAULT_PAGE,
    isFetchingAlbums: false,
    error: null
  }),
  [FETCH_ALBUMS_FAILURE]: (state, action) => ({
    ...state,
    isFetchingAlbums: false,
    error: action.payload
  }),
  [FETCH_ALBUM_PHOTOS_REQUEST]: (state) => ({
    ...state,
    isFetchingAlbumPhotos: true,
    error: null
  }),
  [FETCH_ALBUM_PHOTOS_SUCCESS]: (state, { payload }) => ({
    ...state,
    ...payload,
    isFetchingAlbumPhotos: false,
    error: null
  }),
  [FETCH_ALBUM_PHOTOS_FAILURE]: (state, action) => ({
    ...state,
    isFetchingAlbumPhotos: false,
    error: action.payload
  }),
  [SET_ALBUMS_CURRENT_PAGE]: (state, { payload }) => ({
    ...state,
    albumsCurrentPage: payload
  }),
  [SET_ALBUM_CURRENT_PAGE]: (state, { payload }) => ({
    ...state,
    albumCurrentPage: payload
  })
}, {
  albums: [],
  albumsCurrentPage: DEFAULT_PAGE,
  albumsPageCount: null,
  isFetchingAlbums: true,
  albumPhotos: [],
  albumCurrentPage: DEFAULT_PAGE,
  albumPageCount: null,
  isFetchingAlbumPhotos: true,
  error: null
});

export default albumsReducer;
