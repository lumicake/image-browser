import { handleActions } from 'redux-actions';
import {
  FETCH_PHOTOS_REQUEST,
  FETCH_PHOTOS_SUCCESS,
  FETCH_PHOTOS_FAILURE,
  SET_PHOTOS_CURRENT_PAGE
} from '../actions/types';
import { DEFAULT_PAGE } from '../config';

const photosReducer = handleActions(
  {
    [FETCH_PHOTOS_REQUEST]: (state, action) => ({
      ...state,
      isFetchingPhotos: true,
      error: null
    }),
    [FETCH_PHOTOS_SUCCESS]: (state, { payload }) => ({
      ...state,
      ...payload,
      isFetchingPhotos: false,
      error: null
    }),
    [FETCH_PHOTOS_FAILURE]: (state, action) => ({
      ...state,
      photos: [],
      currentPage: null,
      pageCount: null,
      isFetchingPhotos: false,
      error: action.payload
    }),
    [SET_PHOTOS_CURRENT_PAGE]: (state, { payload }) => ({
      ...state,
      currentPage: payload
    })
  },
  {
    photos: [],
    pageCount: null,
    currentPage: DEFAULT_PAGE,
    error: null,
    isFetchingPhotos: true
  }
);

export default photosReducer;
