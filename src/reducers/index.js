import { combineReducers } from 'redux';
import photosReducer from './photos';
import photoReducer from './photo';
import albumsReducer from './albums';

export default combineReducers({
  photos: photosReducer,
  photo: photoReducer,
  albums: albumsReducer
});
