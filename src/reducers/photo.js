import { handleActions } from 'redux-actions';
import { FETCH_PHOTO_REQUEST, FETCH_PHOTO_SUCCESS, FETCH_PHOTO_FAILURE } from '../actions/types';
import { 
  FETCH_METADATA_REQUEST,
  FETCH_METADATA_SUCCESS,
  FETCH_METADATA_FAILURE,
  FETCH_METADATA_PARTIAL_SUCCESS
} from '../actions/types';

const photoReducer = handleActions(
  {
    [FETCH_PHOTO_REQUEST]: (state, action) => ({
      ...state,
      photo: null,
      isFetchingPhoto: true,
      error: null
    }),
    [FETCH_PHOTO_SUCCESS]: (state, action) => ({
      ...state,
      photo: action.payload,
      isFetchingPhoto: false,
      error: null
    }),
    [FETCH_PHOTO_FAILURE]: (state, action) => ({
      ...state,
      photo: null,
      isFetchingPhoto: false,
      error: action.payload
    }),
    [FETCH_METADATA_REQUEST]: (state) => ({
      ...state,
      metadata: {
        ...state.metadata,
        isFetchingMetadata: true,
        error: null
      }
    }),
    [FETCH_METADATA_SUCCESS]: (state, action) => ({
      ...state,
      metadata: {
        ...state.metadata,
        ...action.payload,
        isFetchingMetadata: false,
        error: null
      }
    }),
    [FETCH_METADATA_PARTIAL_SUCCESS]: (state, action) => ({
      ...state,
      metadata: {
        ...state.metadata,
        ...action.payload.metadata,
        isFetchingMetadata: false,
        error: action.payload.error
      }
    }),
    [FETCH_METADATA_FAILURE]: (state, action) => ({
      ...state,
      metadata: {
        ...state.metadata,
        isFetchingMetadata: false,
        error: action.payload
      }
    })
  },
  {
    photo: null,
    metadata: {
      isFetchingMetadata: true,
      error: null
    },
    error: null,
    isFetchingPhoto: true
  }
);

export default photoReducer;
