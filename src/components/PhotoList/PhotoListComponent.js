import React from 'react';
import '../../style/photolist.css';
import ImageComponent from './ImageComponent';

const PhotoListComponent = ({ photos }) => (
  <ul className="photo-list">
    {photos.map((image) => (
      <li className="photo-list-item" key={image.id}>
        <ImageComponent imageId={image.id} thumbnailUrl={image.thumbnailUrl}/>
      </li>
    ))}
  </ul>
);

export default PhotoListComponent;
