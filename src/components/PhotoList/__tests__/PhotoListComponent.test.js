import React from 'react';
import { shallow } from 'enzyme';
import PhotoListComponent from '../PhotoListComponent';
import ImageComponent from '../ImageComponent';

const MOCK_PHOTOS_DATA = [
  {
    id: 1,
    thumbnailUrl: 'https://via.placeholder.com/150/92c952'
  },
  {
    id: 2,
    thumbnailUrl: 'https://via.placeholder.com/150/771796'
  },
  {
    id: 3,
    thumbnailUrl: 'https://via.placeholder.com/150/24f355'
  }
];

it('should display a list of photos', () => {
  const wrapped = shallow(<PhotoListComponent photos={MOCK_PHOTOS_DATA}/>);
  expect(wrapped.find('ul').length).toEqual(1);
  expect(wrapped.find('ul').childAt(0).childAt(0).type()).toEqual(ImageComponent);
  expect(wrapped.find('ul').children()).toHaveLength(3);
});
