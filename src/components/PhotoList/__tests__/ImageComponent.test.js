import React from 'react';
import { render, shallow } from 'enzyme';
import ImageComponent from '../ImageComponent';
import { wrapWithRouter } from '../../../utils/testUtils';

const MOCK_IMAGEID = 52;
const MOCK_THUMBNAIL_URL = "https://via.placeholder.com/150/92c952";

it('should contain an image', () => {
  const wrapped = render(wrapWithRouter(
    <ImageComponent
      thumbnailUrl={MOCK_THUMBNAIL_URL}
      imageId={MOCK_IMAGEID}
    />));

  expect(wrapped.find('img')).toHaveLength(1);
});

it('should contain a link to the photo', () => {
  const wrapped = shallow(<ImageComponent
    thumbnailUrl={MOCK_THUMBNAIL_URL}
    imageId={MOCK_IMAGEID}
  />);

  const link = wrapped.find('Link');
  expect(link.props().to).toContain(MOCK_IMAGEID);
});
