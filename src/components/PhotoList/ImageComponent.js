import { Link } from 'react-router-dom';
import Image from 'react-bootstrap/Image';
import React from 'react';

export default ({ imageId, thumbnailUrl }) => (
  <Link to={`/photos/${imageId}`}>
    <Image src={thumbnailUrl} rounded/>
  </Link>
);
