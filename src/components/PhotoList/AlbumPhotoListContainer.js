import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FETCH_ALBUM_PHOTOS_REQUEST, SET_ALBUM_CURRENT_PAGE } from '../../actions/types';
import Spinner from '../Spinner/Spinner';
import PhotoListComponent from './PhotoListComponent';
import Pagination from '../Pagination/Pagination';
import { dispatchNewFetchIfPageHasChanged } from '../../utils/utils';
import { partial } from 'ramda';

class AlbumPhotoListContainer extends Component {
  componentDidMount() {
    const { fetchAlbumPhotos, match, currentPage } = this.props;
    if (match) {
      fetchAlbumPhotos(match.params.albumId, currentPage);
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { fetchAlbumPhotos, match } = this.props;
    const fetchFunction = partial(fetchAlbumPhotos, [match.params.albumId]);
    dispatchNewFetchIfPageHasChanged(fetchFunction, prevProps, this.props);
  }

  handlePageChange = ({ selected }) => {
    this.props.setCurrentPage(selected + 1);
  };

  render() {
    const { photos, isFetchingPhotos, pageCount, currentPage } = this.props;
    return isFetchingPhotos ? <Spinner/> :
      (
        <div>
          <PhotoListComponent photos={photos}/>
          {pageCount && <Pagination
            pageCount={pageCount}
            initialPage={currentPage - 1}
            onPageChange={this.handlePageChange}
          />}
        </div>
      );
  }
}

const mapStateToProps = ({ albums }) => ({
  photos: albums.albumPhotos,
  isFetchingPhotos: albums.isFetchingAlbumPhotos,
  currentPage: albums.albumCurrentPage,
  pageCount: albums.albumPageCount
});

const mapDispatchToProps = {
  fetchAlbumPhotos: (albumId, pageNumber) => ({
      type: FETCH_ALBUM_PHOTOS_REQUEST,
      payload: { albumId, pageNumber }
  }),
  setCurrentPage: (newPage) => ({ type: SET_ALBUM_CURRENT_PAGE, payload: newPage })
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumPhotoListContainer);
