import React, { Component } from 'react';
import { connect } from 'react-redux';
import PhotoListComponent from './PhotoListComponent';
import { FETCH_PHOTOS_REQUEST, SET_PHOTOS_CURRENT_PAGE } from '../../actions/types';
import Spinner from '../Spinner/Spinner';
import '../../style/pagination.css';
import Pagination from '../Pagination/Pagination';
import { dispatchNewFetchIfPageHasChanged } from '../../utils/utils';

class PhotoListContainer extends Component {
  componentDidMount() {
    const { currentPage, fetchPhotos } = this.props;
    fetchPhotos(currentPage);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    dispatchNewFetchIfPageHasChanged(this.props.fetchPhotos, prevProps, this.props);
  }

  handlePageChange = ({ selected }) => {
    this.props.setCurrentPage(selected + 1);
  };

  render() {
    const { isFetchingPhotos, photos, pageCount, currentPage } = this.props;

    let component = <Spinner/>;
    if (!isFetchingPhotos) {
      component = (
        <div>
          <PhotoListComponent photos={photos}/>
          {pageCount !== null ? <Pagination
            pageCount={pageCount}
            onPageChange={this.handlePageChange}
            initialPage={currentPage - 1}
          /> : null}
        </div>
      );
    }

    return component;
  }
}

const mapStateToProps = ({ photos }) => ({
  isFetchingPhotos: photos.isFetchingPhotos,
  photos: photos.photos,
  pageCount: photos.pageCount,
  currentPage: photos.currentPage
});

const mapDispatchToProps = {
  fetchPhotos: (pageNumber) => ({ type: FETCH_PHOTOS_REQUEST, payload: pageNumber }),
  setCurrentPage: (pageNumber) => ({ type: SET_PHOTOS_CURRENT_PAGE, payload: pageNumber })
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoListContainer);
