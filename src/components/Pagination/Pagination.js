import React from 'react';
import ReactPaginate from 'react-paginate';
import '../../style/pagination.css';

const Pagination = ({ ...props }) => {
  return (<ReactPaginate
    marginPagesDisplayed={1}
    pageRangeDisplayed={5}
    containerClassName="pagination-container"
    breakClassName="pagination-list-item pagination-page-item"
    pageClassName="pagination-list-item pagination-page-item"
    previousClassName="pagination-list-item prev-and-next"
    nextClassName="pagination-list-item prev-and-next"
    pageLinkClassName="pagination-link"
    activeLinkClassName="pagination-link"
    breakLinkClassName="pagination-link"
    previousLinkClassName="pagination-link"
    nextLinkClassName="pagination-link"
    {...props}
  />)
};

export default Pagination;
