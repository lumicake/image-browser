import React from 'react';
import { render } from 'enzyme';
import HeaderComponent from '../HeaderComponent';
import { wrapWithRouter } from '../../../utils/testUtils';

it('should contain navigation links', () => {
  const wrapped = render(wrapWithRouter(<HeaderComponent/>));
  expect(wrapped.find('a').length).toEqual(3);
});
