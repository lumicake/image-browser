import React from 'react';
import '../../style/header.css';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { IndexLinkContainer, LinkContainer } from "react-router-bootstrap";


export default () => (
  <header>
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand>photo-browser</Navbar.Brand>
      <Nav className="mr-auto">
        <IndexLinkContainer activeClassName="activeNavLink" to="/">
          <Nav.Link>Home</Nav.Link>
        </IndexLinkContainer>
        <LinkContainer activeClassName="activeNavLink" to="/photos">
          <Nav.Link>Photos</Nav.Link>
        </LinkContainer>
        <LinkContainer activeClassName="activeNavLink" to="/albums">
          <Nav.Link>Albums</Nav.Link>
        </LinkContainer>
      </Nav>
    </Navbar>
  </header>
);
