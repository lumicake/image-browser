import React, { Component } from 'react';
import '../style/App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import HeaderContainer from './Header/HeaderContainer';
import PhotoListContainer from './PhotoList/PhotoListContainer';
import FullPhotoContainer from './FullPhoto/FullPhotoContainer';
import AlbumListContainer from './AlbumList/AlbumListContainer';
import AlbumPhotoListContainer from './PhotoList/AlbumPhotoListContainer';

const HomeComponent = () => (
  <div className="home-content">
    <h1>Welcome to photo-browser!</h1>
    <p>To get started, click on Photos or Albums.</p>
  </div>
);

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <HeaderContainer/>
          <Route exact path="/" component={HomeComponent}/>
          <Route exact path="/photos" component={PhotoListContainer}/>
          <Route path="/photos/:photoId" component={FullPhotoContainer}/>
          <Route exact path="/albums" component={AlbumListContainer}/>
          <Route path="/albums/:albumId" component={AlbumPhotoListContainer}/>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;
