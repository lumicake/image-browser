import React from 'react';
import '../../style/App.css';

const Spinner = ({ color = '#c5cae9' }) => (
  <div className="spinner-custom-background">
    <div className="spinner-border" style={{color}} role="status">
      <span className="sr-only">Loading...</span>
    </div>
  </div>
);

export default Spinner;
