import React, { Component } from 'react';
import FullPhotoComponent from './FullPhotoComponent';
import { FETCH_PHOTO_REQUEST } from '../../actions/types';
import { connect } from 'react-redux';
import Spinner from '../Spinner/Spinner';
import MetadataContainer from './Metadata/MetadataContainer';


class FullPhotoContainer extends Component {
  componentDidMount() {
    const { fetchPhoto, match } = this.props;
    if (match) {
      fetchPhoto(match.params.photoId);
    }
  }

  render() {
    const { isFetchingPhoto, photo } = this.props;
    const photoNotAvailable = isFetchingPhoto || !photo.photo;
    let component = <Spinner/>;
    if (!photoNotAvailable) {
      component = (
        <div>
          <FullPhotoComponent photoUrl={photo.photo.url}/>
          <MetadataContainer photo={photo.photo}/>
        </div>
      )
    }
    return component;
  }
}

const mapStateToProps = ({ photo }) => ({
  photo
});

const mapDispatchToProps = {
  fetchPhoto: (photoId) => ({ type: FETCH_PHOTO_REQUEST, payload: photoId })
};

export default connect(mapStateToProps, mapDispatchToProps)(FullPhotoContainer);
