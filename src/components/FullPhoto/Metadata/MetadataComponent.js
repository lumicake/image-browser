import React from 'react';
import '../../../style/metadata.css';
import ErrorDisplay from '../../ErrorComponent';
import { Link } from 'react-router-dom';

const CenteredTextRow = ({ fieldName, fieldValue }) => (
  <div className="centered-text-row">
    <div className="left half-text-field"><strong>{fieldName}</strong></div>
    <div className="right half-text-field">{fieldValue}</div>
  </div>
);

const UserMetadata = ({ userData }) => (
  <div>
    <CenteredTextRow fieldName="by" fieldValue={userData.name}/>
    <CenteredTextRow fieldName="username" fieldValue={userData.username}/>
    <CenteredTextRow fieldName="email" fieldValue={userData.email}/>
    <CenteredTextRow fieldName="www" fieldValue={userData.website}/>
  </div>
);

const AlbumMetadata = ({ albumData }) => (
  <div>
    <CenteredTextRow fieldName="album" fieldValue={
      <Link to={`/albums/${albumData.id}`}>{albumData.title}</Link>
    }/>
  </div>
);

const MetadataComponent = ({ title, userData, albumData, errorData }) => (
  <footer className="metadata-footer">
    <div className="metadata-top">
      <div>{title}</div>
    </div>
    <div className="metadata-bottom">
      <div className="metadata-user">
        {
          userData ?
            <UserMetadata userData={userData}/> :
            <ErrorDisplay errorMessage={errorData}/>
        }
      </div>
      <div className="metadata-album">
        {
          albumData ?
            <AlbumMetadata albumData={albumData}/> :
            <ErrorDisplay errorMessage={errorData}/>
        }
      </div>
    </div>
  </footer>
);

export default MetadataComponent;
