import React, { Component } from 'react';
import { connect } from 'react-redux';
import MetadataComponent from './MetadataComponent';
import { FETCH_METADATA_REQUEST } from '../../../actions/types';
import Spinner from '../../Spinner/Spinner';


class MetadataContainer extends Component {
  componentDidMount() {
    const { fetchMetadata, photo } = this.props;
    if (photo) {
      fetchMetadata(photo.albumId);
    }
  }

  render() {
    const { photo, metadata } = this.props;
    return metadata.isFetchingMetadata ? <Spinner/> :
      <MetadataComponent
        title={photo.title}
        userData={metadata.user}
        albumData={metadata.album}
        errorData={metadata.error}
      />;
  }
}

const mapStateToProps = ({ photo }) => ({
  metadata: photo.metadata
});

const mapDispatchToProps = {
  fetchMetadata: (albumId) => ({ type: FETCH_METADATA_REQUEST, payload: albumId })
};

export default connect(mapStateToProps, mapDispatchToProps)(MetadataContainer);
