import React from 'react';
import { render } from 'enzyme';
import MetadataComponent from '../MetadataComponent';
import { wrapWithRouter } from '../../../../utils/testUtils';

const MOCK_TITLE_DATA = 'testy testperson\'s most awesome photo';

const MOCK_USER_DATA = {
  name: 'Testy Testperson',
  username: 'xXtest89Xx',
  website: 'testperson.test',
  email: 'testy@testperson.test'
};

const MOCK_ALBUM_DATA = {
  id: 1,
  title: 'testy album'
};

const MOCK_ERROR_DATA = 'OH_NO_SOMETHING_IS_WRONG';

it('should display the title of the photo', () => {
  const wrapped = render(<MetadataComponent title={MOCK_TITLE_DATA}/>);

  expect(wrapped.text()).toContain(MOCK_TITLE_DATA);
});

it('should display user data', () => {
  const wrapped = render(<MetadataComponent userData={MOCK_USER_DATA}/>);

  expect(wrapped.text()).toContain(MOCK_USER_DATA.name);
  expect(wrapped.text()).toContain(MOCK_USER_DATA.username);
  expect(wrapped.text()).toContain(MOCK_USER_DATA.website);
  expect(wrapped.text()).toContain(MOCK_USER_DATA.email);
});

it('should display album data', () => {
  const wrapped = render(wrapWithRouter(<MetadataComponent albumData={MOCK_ALBUM_DATA}/>));

  expect(wrapped.text()).toContain(MOCK_ALBUM_DATA.title);
});

it('should contain a link in album data', () => {
  const wrapped = render(wrapWithRouter(<MetadataComponent albumData={MOCK_ALBUM_DATA}/>));
  const albumData = wrapped.find('.metadata-album');

  expect(albumData.find('a')).toHaveLength(1);
});

it('should display error data in the user data section if user data is null', () => {
  const wrapped = render(wrapWithRouter(
    <MetadataComponent
      userData={null}
      albumData={MOCK_ALBUM_DATA}
      errorData={MOCK_ERROR_DATA}
    />
    ));
  const userData = wrapped.find('.metadata-user');

  expect(userData.text()).toContain(MOCK_ERROR_DATA);
});

it('should display error data in the album data section if album data is null', () => {
  const wrapped = render(
    <MetadataComponent
      userData={MOCK_USER_DATA}
      albumData={null}
      errorData={MOCK_ERROR_DATA}
    />
  );
  const albumData = wrapped.find('.metadata-album');

  expect(albumData.text()).toContain(MOCK_ERROR_DATA);
});
