import React from 'react';
import { render } from 'enzyme';
import FullPhotoComponent from '../FullPhotoComponent';

it('should contain an image', () => {
  const wrapped = render(<FullPhotoComponent photo={{ url: 'https://via.placeholder.com/600/24f355' }}/>);
  expect(wrapped.find('img')).toHaveLength(1);
});
