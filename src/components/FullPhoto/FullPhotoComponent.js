import React from 'react';
import Image from 'react-bootstrap/Image';

const FullPhotoComponent = ({ photoUrl }) => (
  <div>
    <Image src={photoUrl}/>
  </div>
);

export default FullPhotoComponent;
