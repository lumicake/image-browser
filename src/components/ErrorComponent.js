import React from 'react';

const ErrorDisplayer = ({ errorMessage }) => (
  <div>
    <p>Oh no! Something went wrong!<br/>
      If you want to help, give this error message to whoever
      is in the highest position of authority:
    </p>
    <p>{errorMessage}</p>
  </div>
);

export default ErrorDisplayer;
