import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FETCH_ALBUMS_REQUEST, SET_ALBUMS_CURRENT_PAGE } from '../../actions/types';
import AlbumListComponent from './AlbumListComponent';
import Pagination from '../Pagination/Pagination';
import { dispatchNewFetchIfPageHasChanged } from '../../utils/utils';
import Spinner from '../Spinner/Spinner';

class AlbumListContainer extends Component {
  componentDidMount() {
    const { fetchAlbums, currentPage } = this.props;
    fetchAlbums(currentPage);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    dispatchNewFetchIfPageHasChanged(this.props.fetchAlbums, prevProps, this.props);
  }

  handlePageChange = ({ selected }) => {
    this.props.setCurrentPage(selected + 1);
  };

  render() {
    const { albums, currentPage, pageCount, isFetchingAlbums } = this.props;
    return isFetchingAlbums ? <Spinner/> : (
      <div>
        <AlbumListComponent albums={albums}/>
        { pageCount && <Pagination
          pageCount={pageCount}
          onPageChange={this.handlePageChange}
          initialPage={currentPage - 1}/>
        }
      </div>);
  }
}

const mapStateToProps = ({ albums }) => ({
  albums: albums.albums,
  isFetchingAlbums: albums.isFetchingAlbums,
  currentPage: albums.albumsCurrentPage,
  pageCount: albums.albumsPageCount
});

const mapDispatchToProps = {
  fetchAlbums: (pageNumber) => ({ type: FETCH_ALBUMS_REQUEST, payload: pageNumber }),
  setCurrentPage: (newPage) => ({ type: SET_ALBUMS_CURRENT_PAGE, payload: newPage })
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumListContainer);
