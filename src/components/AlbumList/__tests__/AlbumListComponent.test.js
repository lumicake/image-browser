import React from 'react';
import { shallow } from 'enzyme';
import AlbumListComponent from '../AlbumListComponent';
import { AlbumComponent } from '../AlbumComponent';

const MOCK_ALBUMS_DATA = [
  {
    userId: 1,
    id: 1,
    title: 'quidem molestiae enim'
  },
  {
    userId: 1,
    id: 2,
    title: 'sunt qui excepturi placeat culpa'
  },
  {
    userId: 1,
    id: 3,
    title: 'omnis laborum odio'
  }
];

it('should display a list of albums', () => {
  const wrapped = shallow(<AlbumListComponent albums={MOCK_ALBUMS_DATA}/>);
  expect(wrapped.find('ul').length).toEqual(1);
  expect(wrapped.find('ul').childAt(0).childAt(0).type()).toEqual(AlbumComponent);
  expect(wrapped.find('ul').children()).toHaveLength(3);
});
