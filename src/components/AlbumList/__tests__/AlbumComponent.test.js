import React from 'react';
import { render, shallow } from 'enzyme';
import { AlbumComponent } from '../AlbumComponent';
import { wrapWithRouter } from '../../../utils/testUtils';

const MOCK_TITLE_SHORT = 'under 20 chars';
const MOCK_TITLE_LONG = 'this title is over 20 characters long';
const MOCK_ALBUMID = 26;

it('should display the name of the album', () => {
  const wrapped = render(wrapWithRouter(
    <AlbumComponent title={MOCK_TITLE_SHORT} albumId={MOCK_ALBUMID}/>
  ));

  expect(wrapped.text()).toContain(MOCK_TITLE_SHORT);
});

it('should shorten the name of the album if it is too long', () => {
  const wrapped = render(wrapWithRouter(
    <AlbumComponent title={MOCK_TITLE_LONG} albumId={MOCK_ALBUMID}/>
  ));

  expect(wrapped.text()).toContain(MOCK_TITLE_LONG.slice(0, 16));
});

it('should contain a link to the album', () => {
  const wrapped = shallow(<AlbumComponent title={MOCK_TITLE_SHORT} albumId={MOCK_ALBUMID}/>);

  const link = wrapped.find('Link');
  expect(link).toHaveLength(1);
  expect(link.prop('to')).toContain(MOCK_ALBUMID);
});
