import React from 'react';
import { Link } from 'react-router-dom';
import { IoIosAlbums } from 'react-icons/io';

export const AlbumComponent = ({ albumId, title }) => (
  <Link to={`/albums/${albumId}`}>
    <div className="album-info-container">
      <IoIosAlbums className="album-svg"/>
      <div className="album-top-text">
        {title.length > 20 ? `${title.slice(0,16)}...` : title}
      </div>
    </div>
  </Link>
);
