import React from 'react';
import '../../style/albumlist.css';
import { AlbumComponent } from './AlbumComponent';


const AlbumListComponent = ({ albums }) => (
  <ul className="album-list">
    {albums.map((album) => (
      <li className="album-list-item" key={album.id}>
        <AlbumComponent albumId={album.id} title={album.title}/>
      </li>
    ))}
  </ul>
);

export default AlbumListComponent;
