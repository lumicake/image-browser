import { FETCH_PHOTOS_REQUEST, FETCH_PHOTOS_SUCCESS, FETCH_PHOTOS_FAILURE } from '../actions/types';
import { call, put, takeEvery } from 'redux-saga/effects';
import { fetchPhotosFromAPI } from '../utils/api';
import { getPageCountFromHeaders } from '../utils/utils';


function* fetchPhotos({ payload }) {
  try {
    const response = yield call(fetchPhotosFromAPI, { page: payload });
    const pageCount = getPageCountFromHeaders(response.headers);

    yield put({
      type: FETCH_PHOTOS_SUCCESS,
      payload: { photos: response.data, currentPage: payload, pageCount }
    });
  } catch (e) {
    yield put({ type: FETCH_PHOTOS_FAILURE, payload: e });
  }
}

export default function* watchFetchPhotos() {
  yield takeEvery(FETCH_PHOTOS_REQUEST, fetchPhotos);
}
