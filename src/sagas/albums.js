import { call, takeEvery, put } from 'redux-saga/effects';
import {
  FETCH_ALBUM_PHOTOS_FAILURE,
  FETCH_ALBUM_PHOTOS_REQUEST,
  FETCH_ALBUM_PHOTOS_SUCCESS,
  FETCH_ALBUMS_FAILURE,
  FETCH_ALBUMS_REQUEST,
  FETCH_ALBUMS_SUCCESS
} from '../actions/types';
import { fetchAlbumsFromAPI, fetchPhotosFromAPI } from '../utils/api';
import { getPageCountFromHeaders, isNumber } from '../utils/utils';


function* fetchAlbums({ payload }) {
  try {
    const response = yield call(fetchAlbumsFromAPI, { page: payload });
    const pageCount = getPageCountFromHeaders(response.headers);

    yield put({
      type: FETCH_ALBUMS_SUCCESS,
      payload: {
        albums: response.data,
        albumsCurrentPage: payload,
        albumsPageCount: pageCount
      }
    });
  } catch (e) {
    yield put({ type: FETCH_ALBUMS_FAILURE, payload: e });
  }
}

export function* watchFetchAlbums() {
  yield takeEvery(FETCH_ALBUMS_REQUEST, fetchAlbums);
}


function* fetchAlbumPhotos({ payload }) {
  const albumId = Number(payload.albumId);
  if (!isNumber(albumId)) {
    yield put({ type: FETCH_ALBUM_PHOTOS_FAILURE, payload: 'INVALID_ALBUMID' });
    return;
  }

  const response = yield call(fetchPhotosFromAPI, { albumId, page: payload.pageNumber });
  if (response.data.length === 0) {
    yield put({ type: FETCH_ALBUM_PHOTOS_FAILURE, payload: 'ALBUM_BY_ALBUMID_DOES_NOT_EXIST' });
    return;
  }

  const pageCount = getPageCountFromHeaders(response.headers);

  yield put({
    type: FETCH_ALBUM_PHOTOS_SUCCESS,
    payload: {
      albumPhotos: response.data,
      albumCurrentPage: payload.pageNumber,
      albumPageCount: pageCount
    }
  });
}

export function* watchFetchAlbumPhotos() {
  yield takeEvery(FETCH_ALBUM_PHOTOS_REQUEST, fetchAlbumPhotos);
}
