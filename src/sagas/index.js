import { all } from 'redux-saga/effects';
import watchFetchPhotos from './photos';
import { watchFetchPhoto, watchFetchMetadata } from './photo';
import { watchFetchAlbums, watchFetchAlbumPhotos } from './albums';

export default function* rootSaga() {
  yield all([
    watchFetchPhotos(),
    watchFetchPhoto(),
    watchFetchMetadata(),
    watchFetchAlbums(),
    watchFetchAlbumPhotos()
  ])
}
