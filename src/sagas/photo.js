import {
  FETCH_PHOTO_REQUEST,
  FETCH_PHOTO_FAILURE,
  FETCH_PHOTO_SUCCESS,
  FETCH_METADATA_REQUEST,
  FETCH_METADATA_SUCCESS,
  FETCH_METADATA_FAILURE,
  FETCH_METADATA_PARTIAL_SUCCESS
} from '../actions/types';
import { call, put, takeEvery, select } from 'redux-saga/effects';
import { find, propEq } from 'ramda';
import { fetchPhotoFromAPI, fetchAlbumDataFromAPI, fetchUserDataFromAPI } from '../utils/api';
import { isNumber } from '../utils/utils';


const selectPhotos = (state) => (
  state.photos.photos
);


function* fetchPhoto(action) {
  const photoId = Number(action.payload);
  if (isNumber(photoId)) {
    const photos = yield select(selectPhotos);
    let photo = find(propEq('id', photoId), photos);

    if (photo === undefined) {
      try {
        photo = yield call(fetchPhotoFromAPI, photoId);
      } catch (e) {
        yield put({ type: FETCH_PHOTO_FAILURE, payload: e });
        return;
      }
      if (photo.data.length === 0) {
        yield put({ type: FETCH_PHOTO_FAILURE, payload: 'PHOTO_BY_PHOTOID_DOES_NOT_EXIST' });
        return;
      }

      photo = photo.data[0];
    }

    yield put({ type: FETCH_PHOTO_SUCCESS, payload: photo });
  } else {
    yield put({ type: FETCH_PHOTO_FAILURE, payload: 'INVALID_PHOTOID' });
  }
}

export function* watchFetchPhoto() {
  yield takeEvery(FETCH_PHOTO_REQUEST, fetchPhoto);
}


function* fetchMetadata(action) {
  const albumId = Number(action.payload);
  if (!isNumber(albumId)) {
    yield put({ type: FETCH_METADATA_FAILURE, payload: 'INVALID_ALBUMID' });
    return;
  }

  let albumData = (yield call(fetchAlbumDataFromAPI, albumId)).data;
  if (albumData.length === 0) {
    yield put({ type: FETCH_METADATA_FAILURE, payload: 'ALBUM_BY_ALBUMID_DOES_NOT_EXIST' });
    return;
  }

  albumData = albumData[0];
  let metadata = {
    album: {
      id: albumData.id,
      title: albumData.title
    }
  };

  const userId = Number(albumData.userId);
  if (!isNumber(userId)) {
    yield put({
      type: FETCH_METADATA_PARTIAL_SUCCESS,
      payload: { metadata, error: 'IVALID_USERID' }
    });
    return;
  }

  let userData = (yield call(fetchUserDataFromAPI, userId)).data;
  if (userData.length === 0) {
    yield put({
      type: FETCH_METADATA_PARTIAL_SUCCESS,
      payload: { metadata, error: 'USER_BY_USERID_DOES_NOT_EXIST' }
    });
    return;
  }

  userData = userData[0];
  metadata = {
    ...metadata,
    user: {
      id: userData.id,
      name: userData.name,
      username: userData.username,
      website: userData.website,
      email: userData.email
    }
  };

  yield put({
    type: FETCH_METADATA_SUCCESS,
    payload: metadata
  });
}

export function* watchFetchMetadata() {
  yield takeEvery(FETCH_METADATA_REQUEST, fetchMetadata);
}

