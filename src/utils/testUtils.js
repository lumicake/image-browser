import React from 'react';
import { MemoryRouter } from 'react-router-dom';

export const wrapWithRouter = (component) => (
  <MemoryRouter>
    {component}
  </MemoryRouter>
);
