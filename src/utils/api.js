import axios from 'axios';
import {
  BASE_API_URL,
  PHOTOS_ENDPOINT,
  ALBUMS_ENDPOINT,
  USERS_ENDPOINT,
  LIMIT_PARAM,
  PAGE_PARAM,
  DEFAULT_LIMIT,
  DEFAULT_PAGE
} from '../config';

export const fetchPhotosFromAPI = ({ limit = DEFAULT_LIMIT, albumId = null, page = DEFAULT_PAGE} = { }) => {
  let url = `${BASE_API_URL}${PHOTOS_ENDPOINT}?${LIMIT_PARAM}${limit}&${PAGE_PARAM}${page}`;
  if (albumId !== null) {
    url = `${url}&albumId=${albumId}`;
  }
  return axios.get(url);
};

export const fetchPhotoFromAPI = (photoId) => {
  const url = `${BASE_API_URL}${PHOTOS_ENDPOINT}?id=${photoId}`;
  return axios.get(url);
};

export const fetchAlbumsFromAPI = ({ limit = DEFAULT_LIMIT, page = DEFAULT_PAGE } = { }) => {
  const url = `${BASE_API_URL}${ALBUMS_ENDPOINT}?${LIMIT_PARAM}${limit}&${PAGE_PARAM}${page}`;
  return axios.get(url);
};

export const fetchAlbumDataFromAPI = (albumId) => {
  const url = `${BASE_API_URL}${ALBUMS_ENDPOINT}?id=${albumId}`;
  return axios.get(url);
};

export const fetchUserDataFromAPI = (userId) => {
  const url = `${BASE_API_URL}${USERS_ENDPOINT}?id=${userId}`;
  return axios.get(url);
};
