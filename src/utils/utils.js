import parseLinkHeader from 'parse-link-header'

export const isNumber = (n) => (
  !isNaN(parseFloat(n)) && isFinite(n)
);

export const dispatchNewFetchIfPageHasChanged = (fetchFunction, oldProps, newProps) => {
  if (oldProps.currentPage !== newProps.currentPage) {
    fetchFunction(newProps.currentPage);
  }
};

export const getPageCountFromHeaders = ({ link }) => (
  link && Number(parseLinkHeader(link).last._page)
);
