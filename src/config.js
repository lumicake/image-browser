export const BASE_API_URL = 'http://jsonplaceholder.typicode.com';
export const PHOTOS_ENDPOINT = '/photos';
export const ALBUMS_ENDPOINT = '/albums';
export const USERS_ENDPOINT = '/users';

export const LIMIT_PARAM = '_limit=';
export const PAGE_PARAM = '_page=';

export const DEFAULT_LIMIT = 40;
export const DEFAULT_PAGE = 1;
